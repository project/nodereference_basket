<?php // $Id$

/**
*  @file
*  Page callbacks for the manage basket page where users can reorder or delete items
*  from their baskets
*/

/**
*  Form callback for the "manage basket" form
*/
function nodereference_basket_manage_form($form_state, $node) {
  if ($node->nid != $_SESSION['nodereference_basket_nid']) {
    drupal_set_message(t('You must first activate the basket for this post'));
    drupal_goto('node/'. $node->nid);
  }
  
  $form = array();
  
    foreach ($_SESSION['nodereference_basket_fields'] AS $field_name => $field) {
          $form[$field_name] = array();
      $form[$field_name]['#tree'] = TRUE;
    foreach ($field['values'] as $key => $value) {
      $node = node_load($value['nid']);
      $form[$field_name][$key] = array(
          'title' => array(
            '#type' => 'markup',
            '#value' => $node->title,
          ),
          'weight' => array(
            '#type' => 'weight',
            '#delta' => count($field['values']),
            '#default_value' => $key,
          ),
          'id' => array(
            '#type' => 'hidden',
            '#value' => $node->nid,
          ), 
          'delete' => array(
            '#type' => 'checkbox',
            '#default_value' => FALSE,
          ),     
        );
      }
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );

  
  return $form;
}

/**
*  Form submit callback for nodereference_basket_manage_form()
*/
function nodereference_basket_manage_form_submit($form, $form_state) {
  foreach ($_SESSION['nodereference_basket_fields'] AS $field_name => $field) {
    $new_order = array();
    foreach ($form_state['values'][$field_name] as $result) {
      if (!$result['delete']) {
        $new_order[$result['weight']] = $result['id'];
      }
    }
    unset($_SESSION['nodereference_basket_fields'][$field_name]['values']);
    ksort($new_order);
    foreach ($new_order as $nid) {
      $_SESSION['nodereference_basket_fields'][$field_name]['values'][]['nid'] = $nid;
    }
  }
}

/**
*  Theme function to change nodereference_basket_manage_form()
*  into a draggable table for reordering or deleting items from
*  the current basket.
*/
function theme_nodereference_basket_manage_form($form) {
  
    $header = array('', t('Name'), t('Sort'), t('Delete'));
    $output = '';
    foreach ($_SESSION['nodereference_basket_fields'] AS $field_name => $field) {
    $rows = array();
    foreach (element_children($form[$field_name]) as $key) {
      $element = &$form[$field_name][$key];
        $element['weight']['#attributes']['class'] = 'weight-group';
          $element['itemid']['#attributes']['class'] = 'itemid';
           
          $row = array('');
          $row[] = drupal_render($element['title']) .
                    drupal_render($element['id']);
          $row[] = drupal_render($element['weight']);
          $row[] = drupal_render($element['delete']);
           
          //Add a draggable class to every table row (<tr>)
          $rows[] = array('data' => $row, 'class' => 'draggable');
       
      }
      drupal_add_tabledrag('nodereference-basket-sort-'. $field_name, 'order', 'sibling', 'weight-group');
      $output .= '<h3>'. $field['field']['widget']['label'] .'</h3>';
      $output .= theme('table', $header, $rows, array('id' => 'nodereference-basket-sort-'. $field_name));
    }
    
    $output .= drupal_render($form);
    return $output;
}