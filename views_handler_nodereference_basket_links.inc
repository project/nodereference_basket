<?php // $Id$

/**
*  @file
*  Views handler to render links within views which will add a selected node 
*  in a view result to the active basket.
*/

class views_handler_nodereference_basket_links extends views_handler_field {
  
    function render($values) {
      if ($_SESSION['nodereference_basket_nid']) {
        $add_node = node_load($_SESSION['nodereference_basket_nid']);
        $output = array();
        foreach ($_SESSION['nodereference_basket_fields'] as $field_name => $field) {
          if (nodereference_basket_node_is_referenceable($field, node_load($values->nid))) {
            $output[] = l(t('Add as %field_name', array('%field_name' => $field['field']['widget']['label'])),
                'node/'. $values->nid .'/add_reference/'. $field_name,
                array('html' => TRUE,
                    'query' => array('destination' => $_GET['q'])
                    ));
          }
        }
        if (count($output) == 1) {
          return $output[0];
        }
        elseif (count($output)) {
          return theme('item_list', $output);
        }
      }
      return t('No Selected Post');
    }
    
    function query() {
    
    }
  }