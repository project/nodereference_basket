<?php // $Id$

/**
*  @file 
*  Views hook implementations to tie the basket with views
*/

/**
*  Implementation of hook_views_data()
*/
function nodereference_basket_views_data() {
  $data['nodereference_basket']['table']['group'] = t('Content');
  $data['nodereference_basket']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['nodereference_basket']['nodereference_basket_links'] = array(
    'title' => t('Basket Links'),
    'help' => t('Add the node to the currently active node in the node reference basket.'),
    'field' => array(
      'handler' => 'views_handler_nodereference_basket_links',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  return $data;
}

/**
*  Implementation of hook_views_handlers()
*/
function nodereference_basket_views_handlers() {
   return array(
    'info' => array(
      'path' => drupal_get_path('module', 'nodereference_basket'),
    ),
    'handlers' => array(

      // argument handlers
      'views_handler_nodereference_basket_links' => array(
        'parent' => 'views_handler_field',
      ),
  )
  );
}